import { Injectable } from '@angular/core';
import { ITour } from "../../models/tour";

@Injectable({
  providedIn: 'root'
})
export class TiсketsStorageService {

  private ticketStorage: ITour[]

  constructor() { }

  setStorage(data: ITour[]): void {
    this.ticketStorage = data;
    window.localStorage.setItem('tickets', JSON.stringify(this.ticketStorage));
  }

  getStorage(): ITour[] {
    if(this.ticketStorage){
      return this.ticketStorage;
    }
    else {
      return JSON.parse(window.localStorage.getItem('tickets') as string);
    }

  }
}
