import { Injectable } from '@angular/core';
import { Observable, Subject } from "rxjs";
import { ISettings } from "../../models/settings";

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  private settingsSubject = new Subject<ISettings>()

  constructor() { }

  loadSettings(): Observable<ISettings>{
    const settingsObservable = new Observable<ISettings>(subscriber => {
      const settingsData: ISettings = {
        saveToken: true
      };
      subscriber.next(settingsData)
    });
    return settingsObservable;
  }

  loadSettingsSubject(data: ISettings){
    this.settingsSubject.next(data)
  }

  getSettingsSubjectObservable(): Observable<ISettings>{
    return this.settingsSubject.asObservable()
  }
}
