import { Injectable } from '@angular/core';
import { TicketRestService } from "../ticket-rest/ticket-rest.service";
import { Observable, Subject, map } from "rxjs";
import {ICustomTourData, IGeneralTourData, INearestTour, ITour, ITourLocation, ITourType} from "../../models/tour";

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  private ticketSubject = new Subject<ITourType>()
  readonly ticketType$ = this.ticketSubject.asObservable();

  constructor(private ticketRestService: TicketRestService) { }

  getTickets(): Observable<ITour[]>{
    return this.ticketRestService.getTickets().pipe(
      map(
        (tours) => {
          const singleTours = tours.filter((el) => el.type === "single");
          return tours.concat(singleTours);
        }
      )
    );
  }

  updateTour(type:ITourType): void {
    this.ticketSubject.next(type);
  }

  getError(){
    return this.ticketRestService.getRestError();
  }

  getNearestTickets(): Observable<INearestTour[]>{
    return this.ticketRestService.getNearestTickets();
  }

  getToursLocation(): Observable<ITourLocation[]>{
    return this.ticketRestService.getLocationList();
  }

  transformData(data:INearestTour[], regions: ITourLocation[]): ICustomTourData[]{
    const newTourData: ICustomTourData[] = [];
    data.forEach((el) => {
      const newEl = <ICustomTourData>{...el};
      newEl.region = <ICustomTourData>regions.find((region) => region.id === el.locationId);
      newTourData.push(newEl);
    });
    return newTourData;
  }

  getRandomNearestEvent(type: number): Observable<INearestTour>{
    return this.ticketRestService.getRandomNearestEvent(type)
  }

  sendData(data: IGeneralTourData): Observable<object>{
    return this.ticketRestService.sendData(data);
  }
}
