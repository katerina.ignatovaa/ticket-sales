import { Injectable } from '@angular/core';
import { IUser } from "../../models/user";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private users:IUser[] = [];

  constructor() { }

  checkUser(user:IUser): boolean{
    const isUserExists = this.users.find(el => el.login == user.login)
    if(isUserExists){
      return isUserExists.password === user.password
    }
    const isUserSaved = localStorage.getItem('userData');
    if(isUserSaved){
      return (JSON.parse(isUserSaved).login === user.login) && (JSON.parse(isUserSaved).password === user.password)
    }
    return false
  }

  checkLogin(user:IUser): boolean{
    const isUserExists = this.users.find(el => el.login == user.login)
    return !!isUserExists
  }

  setUser(user:IUser): void{
    const isUserExists = this.users.find(el => el.login == user.login)
    if(!isUserExists){
      this.users.push(user)
    }
  }
}
