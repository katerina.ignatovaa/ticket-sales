import { Injectable } from '@angular/core';
import { IUser } from "../../models/user";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private user: IUser;
  private token: string;

  constructor() { }

  getUser(): IUser {
    return JSON.parse(window.localStorage.getItem('user') as string);
  }

  setUser(user: IUser) {
    this.user = user;
    window.localStorage.setItem('user', JSON.stringify(this.user));
  }

  getToken(): string {
    return JSON.parse(window.localStorage.getItem('token') as string);
  }

  setToken(token: string) {
    this.token = token;
    window.localStorage.setItem('token', JSON.stringify(this.token));
  }

  deleteUserInfo(){
    window.localStorage.removeItem('user');
    window.localStorage.removeItem('token');
  }
}
