import { Injectable } from '@angular/core';
import {StatisticsRestService} from "../statistics-rest/statistics-rest.service";
import { Observable, map } from "rxjs";
import { ICustomUserStatistics } from "../../models/user";

@Injectable({
  providedIn: 'root'
})
export class StatisticsService {

  constructor(private statisticsRestService: StatisticsRestService) { }

  getUserStatistics(): Observable<ICustomUserStatistics[]>{
    return this.statisticsRestService.getUserStatistics().pipe(
      map((data) => {
        const newStatisticsData: ICustomUserStatistics[] = [];
          data.forEach((el) => {
            const newEl: ICustomUserStatistics = {
              name: el.name,
              company: el.company.name,
              phone: el.phone,
              id: el.id,
              city: el.address.city,
              street: el.address.street
            } ;
            newStatisticsData.push(newEl);
          });
        return newStatisticsData;
        }
      )
    )
  }
}
