import {
  Directive,
  ElementRef,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

@Directive({
  selector: '[appBlockStyle]',
  host:{
    '(document:keyup)': 'initKeyUp($event)',
  },
  exportAs: 'blockStyle'
})
export class BlockStyleDirective{

  @Input() selector: string;
  @Output() enterTicket = new EventEmitter;
  private items: HTMLElement[];
  private index: number = 0;
  activeElementIndex: number;

  constructor(private element: ElementRef) { }

  initStyleForSelectedElement() {
    if (this.selector) {
      this.items = this.element.nativeElement.querySelectorAll(this.selector);
      this.items.forEach((el) => {
        (el as HTMLElement).removeAttribute('style');
        (el.querySelector('.item-name') as HTMLElement).style.display = 'none';
        (el.querySelector('.item-price') as HTMLElement).style.display = 'none';
      })
    }
    this.index = Number(window.localStorage.getItem('index'));
    if (this.items[this.index]) {
      this.activeElementIndex = this.index + 1;
      (this.items[this.index] as HTMLElement).scrollIntoView({block: "center", behavior: "smooth"});
      (this.items[this.index] as HTMLElement).setAttribute('style', 'transform: scale(1.1); box-shadow: 0 16px 20px rgba(0, 0, 0, 0.3)');
      (this.items[this.index].querySelector('.item-name') as HTMLElement).style.display = 'flex';
      (this.items[this.index].querySelector('.item-price') as HTMLElement).style.display = 'block';
    }
  }

  initKeyUp(event: KeyboardEvent){
    if(event.key === 'ArrowRight' || event.key === 'ArrowLeft'){
      (this.items[this.index] as HTMLElement).removeAttribute('style');
      (this.items[this.index].querySelector('.item-name') as HTMLElement).style.display = 'none';
      (this.items[this.index].querySelector('.item-price') as HTMLElement).style.display = 'none';
    }
    if(event.key === 'ArrowRight'){
      if(this.index < (this.items.length - 1)){
        this.index ++;
      }
      (this.items[this.index] as HTMLElement).setAttribute('style', 'transform: scale(1.1); box-shadow: 0 16px 20px rgba(0, 0, 0, 0.3)');
      (this.items[this.index].querySelector('.item-name') as HTMLElement).style.display = 'flex';
      (this.items[this.index].querySelector('.item-price') as HTMLElement).style.display = 'block';
    }
    else if(event.key === 'ArrowLeft'){
      if(this.index > 0){
        this.index --;
      }
      (this.items[this.index] as HTMLElement).setAttribute('style', 'transform: scale(1.1); box-shadow: 0 16px 20px rgba(0, 0, 0, 0.3)');
      (this.items[this.index].querySelector('.item-name') as HTMLElement).style.display = 'flex';
      (this.items[this.index].querySelector('.item-price') as HTMLElement).style.display = 'block';
    }
    localStorage.setItem('index', `${this.index}`);
    (this.items[this.index] as HTMLElement).scrollIntoView({block: "center", behavior: "smooth"})
    this.activeElementIndex = this.index + 1;
    if(event.key === 'Enter'){
      this.enterTicket.emit(this.index);
    }
  }

  updateItems(): void {
    this.items = this.element.nativeElement.querySelectorAll(this.selector);
    window.localStorage.setItem('index', '0');
  }
}

