import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { IMenuType } from "../../../models/menuType";
import { ITourType } from "../../../models/tour";
import {TicketsService} from "../../../services/tickets/tickets.service";
import { MessageService } from 'primeng/api';
import {SettingsService} from "../../../services/settings/settings.service";

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss']
})
export class AsideComponent implements OnInit {

  menuTypes: IMenuType[];
  selectedMenuType: IMenuType;
  @Output() updateMenuType: EventEmitter<IMenuType> = new EventEmitter();
  tourTypes: ITourType[] = [
    {label: 'Все', value: 'all'},
    {label: 'Одиночный', value: 'single'},
    {label: 'Групповой', value: 'multi'}
  ]
  date = new Date().toLocaleDateString();

  constructor(private ticketsService: TicketsService,
              private messageService: MessageService,
              private settingsService: SettingsService) {
  }

  ngOnInit(): void {
    this.menuTypes = [
      {type: 'custom', label: 'Обычное'},
      {type: 'extended', label: 'Расширенное'}
    ]
  }

  changeMenuType(ev: { ev: Event, value: IMenuType }): void {
    this.updateMenuType.emit(ev.value);
  }

  changeTourType(ev: { ev: Event, value: ITourType }): void {
    this.ticketsService.updateTour(ev.value)
  }

  selectDate(ev: string) {
    this.ticketsService.updateTour({date: ev})
  }

  clearDate() {
    this.ticketsService.updateTour({date: ''})
  }

  initRestError(): void {
    this.ticketsService.getError().subscribe({
      next: (data) => {},
      error: (err) => {
        console.log('err', err)
      }
    });
    this.messageService.add({severity: 'error', summary: "Ошибка запроса"});
  }

  initSettingsData(): void{
    this.settingsService.loadSettingsSubject({saveToken: false})
  }
}
