import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { TicketsService } from "../../../services/tickets/tickets.service";
import { ITour, ITourType } from "../../../models/tour";
import { TiсketsStorageService } from "../../../services/tiсkets-storage/tiсkets-storage.service";
import { Router } from "@angular/router";
import { BlockStyleDirective } from "../../../directive/block-style.directive";
import { debounceTime, fromEvent, Subscription } from "rxjs";

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.scss']
})
export class TicketListComponent implements OnInit, AfterViewInit, OnDestroy {

  tickets: ITour[] = [];
  @ViewChild('blockDirective') blockDirective: BlockStyleDirective;
  renderComplete: boolean = false;
  index: number;
  private tourUnsubscriber: Subscription;
  ticketsCopy: ITour[];
  ticketSearchValue: string;
  @ViewChild('ticketSearch') ticketSearch: ElementRef;
  private ticketSearchSub: Subscription;

  constructor(private ticketService: TicketsService,
              private ticketStorage: TiсketsStorageService,
              private router: Router) { }

  ngOnInit(): void {
    this.ticketService.getTickets().subscribe(
      (data) => {
        this.tickets = data;
        this.ticketStorage.setStorage(data);
        this.ticketsCopy = [...this.tickets];
        setTimeout(
          () => {
            this.blockDirective.initStyleForSelectedElement();
            this.renderComplete = true;
          }
        )
      }
    )
    this.tourUnsubscriber = this.ticketService.ticketType$.subscribe((data: ITourType) => {
      switch (data.value) {
        case "single":
          this.tickets = this.ticketsCopy.filter((el) => el.type === "single");
          break;
        case "multi":
          this.tickets = this.ticketsCopy.filter((el) => el.type === "multi");
          break;
        case "all":
          this.tickets = [...this.ticketsCopy];
          break;
      }
      if (data.date) {
        const dateWithoutTime = new Date(data.date).toISOString().split('T');
        const dateValue = dateWithoutTime[0];
        this.tickets = this.ticketsCopy.filter((el) => el.date === dateValue);
      }
      else if(!data.value) {
        this.tickets = [...this.ticketsCopy];
      }

      setTimeout(
        () => {
          this.blockDirective.updateItems();
          this.blockDirective.initStyleForSelectedElement();
          this.renderComplete = true;
        }
      )
    });
  }

  ngAfterViewInit() {
    this.ticketSearchSub = fromEvent(this.ticketSearch.nativeElement, 'keyup')
      .pipe(debounceTime(200))
      .subscribe((ev) => {
        if(this.ticketSearchValue){
          this.tickets = this.ticketsCopy.filter((el) => el.name.toLowerCase().includes(this.ticketSearchValue.toLowerCase()))
        } else {
          this.tickets = [...this.ticketsCopy];
        }
        setTimeout(
          () => {
            this.blockDirective.updateItems();
            this.blockDirective.initStyleForSelectedElement();
            this.renderComplete = true;
          }
        )
      }
    );
  }

  ngOnDestroy() {
    this.tourUnsubscriber.unsubscribe();
    this.ticketSearchSub.unsubscribe();
  }

  goToTicketInfoByClick(item: ITour){
    this.index = this.tickets.findIndex(el => el.id === item.id);
    localStorage.setItem('index', `${this.index}`);
    this.router.navigate([`/tickets/ticket`], {queryParams: {id: item.id}})
  }

  goToTicketInfoByEnter(index: number){
    this.router.navigate([`/tickets/ticket`], {queryParams: {id: this.tickets[index].id}})
  }
}
