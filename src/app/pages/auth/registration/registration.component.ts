import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../../services/auth/auth.service";
import { MessageService } from 'primeng/api'
import { IUser } from "../../../models/user";
import {ConfigService} from "../../../services/config/config.service";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  login: string;
  email: string = '';
  password: string;
  passwordConfirm: string;
  cardNumber: string = '';
  isSelected: boolean;
  showCardNumber: boolean;

  constructor(private authService: AuthService,
              private messageService: MessageService) { }

  ngOnInit(): void {
    this.showCardNumber = ConfigService.config.useUserCard;
  }

  register(): void{
    const user: IUser = {
      login: this.login,
      email: this.email,
      password: this.password,
      cardNumber: this.cardNumber
    }
    if(!this.authService.checkLogin(user)){
      if(this.password !== this.passwordConfirm){
        this.messageService.add({severity:'error', summary:"Пароли не совпадают"});
      }
      else{
        this.authService.setUser(user);
        this.messageService.add({severity:'success', summary:"Регистрация прошла успешно"});
        if(this.isSelected){
          localStorage.setItem('userData', JSON.stringify(user));
        }
      }
    }
    else
      this.messageService.add({severity:'warning', summary:"Данный пользователь уже зарегистрирован"});
  }
}
