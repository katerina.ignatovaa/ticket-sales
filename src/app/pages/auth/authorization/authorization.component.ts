import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../../services/auth/auth.service";
import { MessageService } from 'primeng/api';
import { IUser } from "../../../models/user";
import { Router } from "@angular/router";
import { UserService } from "../../../services/user/user.service";
import {ConfigService} from "../../../services/config/config.service";

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.scss']
})

export class AuthorizationComponent implements OnInit {

  login: string;
  password: string;
  isSelected: boolean;
  cardNumber: string = '';
  showCardNumber: boolean

  constructor(private authService: AuthService,
              private userService: UserService,
              private messageService: MessageService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.showCardNumber = ConfigService.config.useUserCard;
  }

  logIn(): void {
    const user: IUser = {
      login: this.login,
      password: this.password,
      cardNumber: this.cardNumber
    }
    if (this.authService.checkUser(user)) {
      this.router.navigate(['tickets/ticket-list']);
      this.userService.setUser(user);
      this.userService.setToken('user_token');
    } else {
      this.messageService.add({severity: 'error', summary: "Неправильный логин или пароль"});
    }
  }
}
