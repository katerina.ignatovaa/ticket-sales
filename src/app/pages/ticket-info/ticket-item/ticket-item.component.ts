import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { INearestTour, ITour, ITourLocation } from "../../../models/tour";
import { ActivatedRoute } from "@angular/router";
import { TiсketsStorageService } from "../../../services/tiсkets-storage/tiсkets-storage.service";
import { IUser } from "../../../models/user";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { UserService } from "../../../services/user/user.service";
import { TicketsService } from "../../../services/tickets/tickets.service";
import { forkJoin, fromEvent, Subscription } from "rxjs";

@Component({
  selector: 'app-ticket-item',
  templateUrl: './ticket-item.component.html',
  styleUrls: ['./ticket-item.component.scss']
})
export class TicketItemComponent implements OnInit, AfterViewInit, OnDestroy {

  ticket: ITour | undefined;
  user: IUser;
  userForm: FormGroup;
  toursLocation: ITourLocation[];
  nearestTours: INearestTour[];
  @ViewChild('ticketSearch') ticketSearch: ElementRef;//
  ticketSearchSub: Subscription;
  searchTypes: number[] = [1, 2, 3];
  ticketRestSub: Subscription;
  nearestToursCopy: INearestTour[];
  ticketSearchValue: string;

  constructor( private route: ActivatedRoute,
               private ticketStorage: TiсketsStorageService,
               private userService: UserService,
               private ticketsService: TicketsService) { }

  ngOnInit(): void {
    this.user = this.userService.getUser();
    this.userForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', [Validators.required, Validators.minLength(2)]),
      cardNumber: new FormControl(),
      birthDay: new FormControl('', Validators.required),
      age: new FormControl('', [Validators.min(1)]),
      citizen: new FormControl()
    })

    const id = this.route.snapshot.queryParamMap.get('id');
    if(id){
      const tickets = this.ticketStorage.getStorage();
      this.ticket = tickets.find(
        (el) => el.id === id
      )
    }

    forkJoin([this.ticketsService.getNearestTickets(), this.ticketsService.getToursLocation()]).subscribe((data) => {
      this.nearestTours = this.ticketsService.transformData(data[0], data[1]);
      this.nearestToursCopy = this.nearestTours;
      this.toursLocation = data[1];
    })
  }

  ngAfterViewInit() {
    this.userForm.controls["cardNumber"].setValue(this.user?.cardNumber);
    this.ticketSearchSub = fromEvent(this.ticketSearch.nativeElement, 'keyup').subscribe((ev) => {
        if(this.ticketSearchValue){
          this.initSearchTours();
        } else {
          this.nearestTours = [...this.nearestToursCopy];
        }
    });
  }

  initSearchTours(){
    const type = Math.floor(Math.random()* this.searchTypes.length);
    if(this.ticketRestSub && !this.ticketSearchSub.closed){
      this.ticketRestSub.unsubscribe();
    }
    this.ticketRestSub = this.ticketsService.getRandomNearestEvent(type).subscribe((data) => {
      this.nearestTours = this.ticketsService.transformData([data], this.toursLocation)
    })
  }

  ngOnDestroy(){
    this.ticketSearchSub.unsubscribe();
  }

  initTour(){
    const userData = this.userForm.getRawValue();
    const generalData = {...this.ticket, ...userData};
    this.ticketsService.sendData(generalData).subscribe();
  }
}
