import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { TabViewModule } from 'primeng/tabview';
import { InputTextModule} from 'primeng/inputtext';
import { FormsModule } from "@angular/forms";
import { ChangePasswordComponent } from './change-password/change-password.component';
import { StatisticsComponent } from './statistics/statistics.component';
import {TableModule} from 'primeng/table';

@NgModule({
  declarations: [
    SettingsComponent,
    ChangePasswordComponent,
    StatisticsComponent
  ],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    ToastModule,
    TabViewModule,
    InputTextModule,
    FormsModule,
    TableModule
  ],
  providers: [
    MessageService
  ]
})
export class SettingsModule { }
