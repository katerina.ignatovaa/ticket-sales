export  interface IUser{
  login: string,
  password: string,
  email?: string,
  cardNumber?: string
}

export interface IUserStatistics {
  id: number,
  name: string,
  username: string,
  email: string,
  address: IUserStatisticsAddress,
  phone: string,
  website: string,
  company: {
    name: string,
    catchPhrase: string,
    bs: string
  }
}
export interface IUserStatisticsAddress {
  street: string,
  suite: string,
  city: string,
  zipcode: string,
  geo: {
    lat: string,
    lng: string
  }
}
export interface ICustomUserStatistics {
  name: string,
  company: string,
  phone: string,
  id: number,
  city: string,
  street: string
}
